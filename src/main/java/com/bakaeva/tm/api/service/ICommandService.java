package com.bakaeva.tm.api.service;

import com.bakaeva.tm.dto.Command;

public interface ICommandService {

    public Command[] getTerminalCommands();

    public String[] getCommands();

    public String[] getArguments();

}