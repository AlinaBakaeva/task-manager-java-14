package com.bakaeva.tm.api.repository;

import com.bakaeva.tm.dto.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();

}